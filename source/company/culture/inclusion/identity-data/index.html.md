---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2019-06-30

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Based in APAC                             | 58    | 8.47%       |
| Based in EMEA                             | 179   | 26.13%      |
| Based in LATAM                            | 14    | 2.04%       |
| Based in NA                               | 434   | 63.36%      |
| Total Team Members                        | 685   | 100%        |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men                                       | 499   | 72.85%      |
| Women                                     | 186   | 27.15%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 685   | 100%        |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men in Leadership                         | 36    | 75.00%      |
| Women in Leadership                       | 12    | 25.00%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 48    | 100%        |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men in Development                        | 263   | 82.97%      |
| Women in Development                      | 54    | 17.03%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 317   | 100%        |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 27    | 6.54%       |
| Black or African American                 | 12    | 2.91%       |
| Hispanic or Latino                        | 27    | 6.54%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.24%       |
| Two or More Races                         | 19    | 4.60%       |
| White                                     | 233   | 56.42%      |
| Unreported                                | 94    | 22.76%      |
| Total Team Members                        | 413   | 100%        |

| Race/Ethnicity in Development (US Only)   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 10    | 7.81%       |
| Black or African American                 | 3     | 2.34%       |
| Hispanic or Latino                        | 8     | 6.25%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 7     | 5.47%       |
| White                                     | 75    | 58.59%      |
| Unreported                                | 25    | 19.53%      |
| Total Team Members                        | 128   | 100%        |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 5     | 12.82%      |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 0     | 0.00%       |
| Native Hawaiian or Other Pacific Islander | 1     | 2.56%       |
| Two or More Races                         | 1     | 2.56%       |
| White                                     | 22    | 56.41%      |
| Unreported                                | 10    | 25.64%      |
| Total Team Members                        | 39    | 100%        |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 53    | 7.74%       |
| Black or African American                 | 18    | 2.63%       |
| Hispanic or Latino                        | 37    | 5.40%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.15%       |
| Two or More Races                         | 25    | 3.65%       |
| White                                     | 369   | 53.87%      |
| Unreported                                | 182   | 26.57%      |
| Total Team Members                        | 685   | 100%        |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 28    | 8.83%       |
| Black or African American                 | 6     | 1.89%       |
| Hispanic or Latino                        | 18    | 5.68%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 11    | 3.47%       |
| White                                     | 172   | 54.26%      |
| Unreported                                | 82    | 25.87%      |
| Total Team Members                        | 317   | 100%        |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 5     | 10.42%      |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 1     | 2.08%       |
| Native Hawaiian or Other Pacific Islander | 1     | 2.08%       |
| Two or More Races                         | 1     | 2.08%       |
| White                                     | 25    | 52.08%      |
| Unreported                                | 15    | 31.25%      |
| Total Team Members                        | 48    | 100%        |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| 18-24                                     | 17    | 2.48%       |
| 25-29                                     | 133   | 19.42%      |
| 30-34                                     | 196   | 28.61%      |
| 35-39                                     | 135   | 19.71%      |
| 40-49                                     | 143   | 20.88%      |
| 50-59                                     | 54    | 7.88%       |
| 60+                                       | 7     | 1.02%       |
| Unreported                                | 0     | 0.00%       |
| Total Team Members                        | 685   | 100%        |
