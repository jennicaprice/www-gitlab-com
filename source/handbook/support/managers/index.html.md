---
layout: markdown_page
title: Support People
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

This section is for information relating to hiring and to the support managers.

## Hiring
See [Support Hiring](hiring.html)

## Metrics

As a Support Management group we should be aware of the tooling we use to generate the metrics that we report on.

1. Periscope
2. Zendesk Explore

## Periscope

Periscope is a general purpose visiualization tool that is used by the greater company.
It's extermely powerful with a full SQL interface to our data across functions.
We work with the data team to generate "Stable" Graphs here. Think, KPIs and greater measures that make sense to report to the larger company.
As managers, we will not need to edit these reports often, but we should consuming it regularly.

## Zendesk Explore

Zendesk Explore is a new tool to replace Zendesk Insights. We will use this tool 
for quick interations on new ideas or fact checking Periscope data. Support Managers
should be ready to work with Explore regularly and be comfortable with the tool.

## Why two metrics systems?

Periscope is a company wide tool that is exteremly powerful which can make it unweildy. 
Explore gives us an interface that is much easier to navigate and use.
Additonally, Periscope data is a secondary source so it can contain errors. 
By being comfortable using ZD Explore, a primary source, we can make sure that we have acurrate data and insights.


## About the Managers
Support Manager ReadMEs:

- [AMER East](support-engineering-manager-readme-americas.html)
